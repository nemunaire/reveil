import { sveltekit } from '@sveltejs/kit/vite';

/** @type {import('vite').UserConfig} */
const config = {
        server: {
            hmr: {
                port: 10000
            }
        },

	plugins: [sveltekit()]
};

export default config;
