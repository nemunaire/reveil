export class Action {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, name, description, path, enabled }) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.path = path;
    this.enabled = enabled;
  }

  async delete() {
    const res = await fetch(`api/actions/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async launch() {
    const res = await fetch(`api/actions/${this.id}/run`, {
      method: 'POST',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  toggleEnable() {
    this.enabled = !this.enabled;
    this.save();
    return this;
  }

  async save() {
    const res = await fetch(this.id?`api/actions/${this.id}`:'api/actions', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getActions() {
  const res = await fetch(`api/actions`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data == null) {
      return []
    } else {
      return data.map((t) => new Action(t));
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getAction(aid) {
  const res = await fetch(`api/actions/${aid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Action(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
