export class Track {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, name, path, enabled }) {
    this.id = id;
    this.name = name;
    this.path = path;
    this.enabled = enabled;
  }

  async delete() {
    const res = await fetch(`api/tracks/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async toggleEnable() {
    this.enabled = !this.enabled;
    return await this.save();
  }

  async save() {
    const res = await fetch(this.id?`api/tracks/${this.id}`:'api/tracks', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return this;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function uploadTrack(files, meta) {
  for (const file of files) {
    const formData = new FormData();
    formData.append("trackfile", file);
    formData.append("meta", JSON.stringify(meta));

    const res = await fetch('/api/tracks', {
      method: 'POST',
      body: formData,
    });
    if (res.ok) {
      const data = await res.json();
      return new Track(data)
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getTracks() {
  const res = await fetch(`api/tracks`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return (await res.json()).map((t) => new Track(t));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getTrack(tid) {
  const res = await fetch(`api/tracks/${tid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Track(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
