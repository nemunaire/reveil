export class Settings {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ language, gong_interval, weather_delay, weather_action, pre_alarm_delay, pre_alarm_action, max_run_time, max_volume, federation }) {
    this.language = language;
    this.gong_interval = gong_interval;
    this.weather_delay = weather_delay;
    this.weather_action = weather_action;
    this.pre_alarm_delay = pre_alarm_delay;
    this.pre_alarm_action = pre_alarm_action;
    this.max_run_time = max_run_time;
    this.max_volume = max_volume;
    this.federation = federation;
  }

  async save() {
    const res = await fetch(`api/settings`, {
      method: 'PUT',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getSettings() {
  const res = await fetch(`api/settings`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Settings(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
