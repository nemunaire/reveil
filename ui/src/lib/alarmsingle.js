export class AlarmSingle {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, time, routines, comment, enable_federation }) {
    this.id = id;
    this.time = new Date(time);
    this.routines = routines == null ? [] : routines;
    this.comment = comment;
    this.enable_federation = enable_federation == true;

    if (this.routines.length < 1) {
      this.routines.push("");
    }
  }

  async delete() {
    const res = await fetch(`api/alarms/single/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save() {
    const res = await fetch(this.id?`api/alarms/single/${this.id}`:'api/alarms/single', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getAlarmsSingle() {
  const res = await fetch(`api/alarms/single`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data === null)
      return [];
    else
      return data.map((t) => new AlarmSingle(t));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getAlarmSingle(aid) {
  const res = await fetch(`api/alarms/single/${aid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new AlarmSingle(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getNextAlarm() {
  const res = await fetch(`api/alarms/next`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data)
      return new Date(data);
    else
      return data;
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function newNCyclesAlarm(nCycles) {
  const res = await fetch('api/alarms/single', {
    method: 'POST',
    headers: {'Accept': 'application/json'},
    body: JSON.stringify({
      time: new Date(Date.now() + 600000 + 5400000 * nCycles)
    }),
  });
  if (res.status == 200) {
    const data = await res.json();
    return new AlarmSingle(data);
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
