export async function isAlarmActive() {
  const res = await fetch('api/alarm', {
    headers: {'Accept': 'application/json'},
  });
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function runAlarm() {
  const res = await fetch('api/alarm/run', {
    method: 'POST',
    headers: {'Accept': 'application/json'},
  });
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function alarmNextTrack() {
  const res = await fetch('api/alarm/next', {
    method: 'POST',
    headers: {'Accept': 'application/json'},
  });
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function deleteNextAlarm() {
  const res = await fetch('api/alarms/next', {
    method: 'DELETE',
    headers: {'Accept': 'application/json'},
  });
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function alarmStop() {
  const res = await fetch('api/alarm', {
    method: 'DELETE',
    headers: {'Accept': 'application/json'},
  });
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
