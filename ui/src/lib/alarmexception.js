export class AlarmException {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, start, end, comment }) {
    this.id = id;
    this.start = start;
    this.end = end;
    this.comment = comment;
  }

  _start() {
    return new Date(this.start);
  }

  _end() {
    return new Date(this.end);
  }

  async delete() {
    const res = await fetch(`api/alarms/exceptions/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save() {
    const res = await fetch(this.id?`api/alarms/exceptions/${this.id}`:'api/alarms/exceptions', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getAlarmsException() {
  const res = await fetch(`api/alarms/exceptions`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data === null)
      return [];
    else
      return data.map((t) => new AlarmException(t));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getAlarmException(aid) {
  const res = await fetch(`api/alarms/exceptions/${aid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new AlarmException(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
