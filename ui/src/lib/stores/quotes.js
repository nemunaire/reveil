import { writable } from 'svelte/store';

function createQuotesStore() {
  const { subscribe, set, update } = writable({quotes: [], quoteOfTheDay: null});

  return {
    subscribe,

    set: (quotes) => {
      update((m) => Object.assign(m, {quotes}));
    },

    setQOTD: (quoteOfTheDay) => {
      update((m) => Object.assign(m, {quoteOfTheDay}));
    },

    refreshQOTD: async () => {
      const res = await fetch(`api/quoteoftheday`, {headers: {'Accept': 'application/json'}})
      if (res.status == 200) {
        const quoteOfTheDay = await res.json();
        update((m) => Object.assign(m, {quoteOfTheDay}));
        return quoteOfTheDay;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    },

    update: (res_quotes, cb=null) => {
      if (res_quotes.status === 200) {
        res_quotes.json().then((quotes) => {
          update((m) => (Object.assign(m, {quotes})));

          if (cb) {
            cb(quotes);
          }
        });
      }
    },
  };

}

export const quotes = createQuotesStore();
