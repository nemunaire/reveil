import { derived, writable } from 'svelte/store';

import { getAlarmsSingle } from '$lib/alarmsingle'

function createAlarmsSingleStore() {
  const { subscribe, set, update } = writable({list: null});

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    clear: () => {
      update((m) => m = {list: null});
    },

    refresh: async () => {
      const list = await getAlarmsSingle();

      update((m) => (Object.assign(m, {list})));
      return list;
    },

    update: (res_AlarmsSingle, cb=null) => {
      if (res_AlarmsSingle.status === 200) {
        res_AlarmsSingle.json().then((list) => {
          update((m) => (Object.assign(m, {list})));

          if (cb) {
            cb(list);
          }
        });
      }
    },
  };

}

export const alarmsSingle = createAlarmsSingleStore();
