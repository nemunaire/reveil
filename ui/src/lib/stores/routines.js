import { writable } from 'svelte/store';

import { getRoutines } from '$lib/routine'

function createRoutinesStore() {
  const { subscribe, set, update } = writable({list: null});

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    refresh: async () => {
      const list = await getRoutines();
      update((m) => Object.assign(m, {list}));
      return list;
    },

    update: (res_routines, cb=null) => {
      if (res_routines.status === 200) {
        res_routines.json().then((list) => {
          update((m) => (Object.assign(m, {list})));

          if (cb) {
            cb(list);
          }
        });
      }
    },
  };

}

export const routines = createRoutinesStore();
