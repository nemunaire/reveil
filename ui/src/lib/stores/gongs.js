import { writable } from 'svelte/store';

import { getGongs } from '$lib/gong'

function createGongsStore() {
  const { subscribe, set, update } = writable({list: null});

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    refresh: async () => {
      const list = await getGongs();
      update((m) => Object.assign(m, {list}));
      return list;
    },

    update: (res_gongs, cb=null) => {
      if (res_gongs.status === 200) {
        res_gongs.json().then((list) => {
          update((m) => (Object.assign(m, {list})));

          if (cb) {
            cb(list);
          }
        });
      }
    },
  };

}

export const gongs = createGongsStore();
