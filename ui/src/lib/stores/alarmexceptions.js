import { derived, writable } from 'svelte/store';

import { getAlarmsException } from '$lib/alarmexception'

function createAlarmsExceptionStore() {
  const { subscribe, set, update } = writable({list: null});

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    clear: () => {
      update((m) => m = {list: null});
    },

    refresh: async () => {
      const list = await getAlarmsException();

      update((m) => (Object.assign(m, {list})));
      return list;
    },

    update: (res_AlarmsException, cb=null) => {
      if (res_AlarmsException.status === 200) {
        res_AlarmsException.json().then((list) => {
          update((m) => (Object.assign(m, {list})));

          if (cb) {
            cb(list);
          }
        });
      }
    },
  };

}

export const alarmsExceptions = createAlarmsExceptionStore();
