import { writable } from 'svelte/store';

import { getTracks } from '$lib/track'

function cmpTracks(a, b) {
  if (a.enabled && !b.enabled) return -1;
  if (!a.enabled && b.enabled) return 1;

  if (a.path.toLowerCase() > b.path.toLowerCase())
    return 1;
  if (a.path.toLowerCase() < b.path.toLowerCase())
    return -1;

  return 0;
}

function createTracksStore() {
  const { subscribe, set, update } = writable({list: null});

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    refresh: async () => {
      const list = await getTracks();
      list.sort(cmpTracks);
      update((m) => Object.assign(m, {list}));
      return list;
    },

    update: (res_tracks, cb=null) => {
      if (res_tracks.status === 200) {
        res_tracks.json().then((list) => {
          update((m) => (Object.assign(m, {list})));

          if (cb) {
            cb(list);
          }
        });
      }
    },
  };

}

export const tracks = createTracksStore();
