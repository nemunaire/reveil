import { derived, writable } from 'svelte/store';

import { getActions } from '$lib/action'

function createActionsStore() {
  const { subscribe, set, update } = writable({list: null, fileIdx: null});

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    refresh: async () => {
      const list = await getActions();
      const fileIdx = {};
      list.forEach(function(action, k) {
	fileIdx[action.path] = action;
      });

      update((m) => (Object.assign(m, {list, fileIdx})));
      return list;
    },

    update: (res_actions, cb=null) => {
      if (res_actions.status === 200) {
        res_actions.json().then((list) => {
	  const fileIdx = {};
	  list.forEach(function(action, k) {
	    fileIdx[action.path] = action;
	  })

          update((m) => (Object.assign(m, {list, fileIdx})));

          if (cb) {
            cb(list);
          }
        });
      }
    },
  };

}

export const actions = createActionsStore();

export const actions_idx = derived(
  actions,
  ($actions) => ($actions.fileIdx),
);
