import { derived, writable } from 'svelte/store';

import { getAlarmsRepeated } from '$lib/alarmrepeated'

function createAlarmsRepeatedStore() {
  const { subscribe, set, update } = writable({list: null});

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    clear: () => {
      update((m) => m = {list: null});
    },

    refresh: async () => {
      const list = await getAlarmsRepeated();

      update((m) => (Object.assign(m, {list})));
      return list;
    },

    update: (res_AlarmsRepeated, cb=null) => {
      if (res_AlarmsRepeated.status === 200) {
        res_AlarmsRepeated.json().then((list) => {
          update((m) => (Object.assign(m, {list})));

          if (cb) {
            cb(list);
          }
        });
      }
    },
  };

}

export const alarmsRepeated = createAlarmsRepeatedStore();
