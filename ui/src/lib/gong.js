export class Gong {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, name, path, enabled }) {
    this.id = id;
    this.name = name;
    this.path = path;
    this.enabled = enabled;
  }

  async delete() {
    const res = await fetch(`api/gongs/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async setDefault() {
    this.enabled = !this.enabled;
    return await this.save();
  }

  async save() {
    const res = await fetch(this.id?`api/gongs/${this.id}`:'api/gongs', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function uploadGong(files, meta) {
  for (const file of files) {
    const formData = new FormData();
    formData.append("gongfile", file);
    formData.append("meta", JSON.stringify(meta));

    const res = await fetch('/api/gongs', {
      method: 'POST',
      body: formData,
    });
    if (res.ok) {
      const data = await res.json();
      return new Gong(data)
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getGongs() {
  const res = await fetch(`api/gongs`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return (await res.json()).map((g) => new Gong(g));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getGong(gid) {
  const res = await fetch(`api/gongs/${gid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Gong(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
