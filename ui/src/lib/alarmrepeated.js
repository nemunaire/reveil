export class AlarmRepeated {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, weekday, time, routines, disabled, ignore_exceptions, comment, excepts, next_time, enable_federation }) {
    this.id = id;
    this.weekday = weekday;
    this.time = time;
    this.routines = routines == null ? [] : routines;
    this.ignore_exceptions = ignore_exceptions;
    this.comment = comment;
    this.disabled = disabled == true;
    this.enable_federation = enable_federation == true;
    if (excepts !== undefined)
      this.excepts = excepts;
    if (next_time !== undefined)
      this.next_time = next_time;

    if (this.routines.length < 1) {
      this.routines.push("");
    }
  }

  async delete() {
    const res = await fetch(`api/alarms/repeated/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save() {
    const res = await fetch(this.id?`api/alarms/repeated/${this.id}`:'api/alarms/repeated', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getAlarmsRepeated() {
  const res = await fetch(`api/alarms/repeated`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data === null)
      return [];
    else
      return data.map((t) => new AlarmRepeated(t));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getAlarmRepeated(aid) {
  const res = await fetch(`api/alarms/repeated/${aid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new AlarmRepeated(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export function weekdayStr(weekday) {
  switch (weekday) {
  case 0:
  case "0":
    return "dimanche";
  case 1:
  case "1":
    return "lundi";
  case 2:
  case "2":
    return "mardi";
  case 3:
  case "3":
    return "mercredi";
  case 4:
  case "4":
    return "jeudi";
  case 5:
  case "5":
    return "vendredi";
  case 6:
  case "6":
    return "samedi";
  }
}
