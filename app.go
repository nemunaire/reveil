package main

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/api"
	"git.nemunai.re/nemunaire/reveil/config"
	"git.nemunai.re/nemunaire/reveil/model"
	"git.nemunai.re/nemunaire/reveil/player"
	"git.nemunai.re/nemunaire/reveil/ui"
)

type App struct {
	cfg          *config.Config
	db           *reveil.LevelDBStorage
	router       *gin.Engine
	srv          *http.Server
	nextAlarm    *time.Timer
	nextPreAlarm *time.Timer
}

func NewApp(cfg *config.Config) *App {
	if cfg.DevProxy == "" {
		gin.SetMode(gin.ReleaseMode)
	}
	gin.ForceConsoleColor()
	router := gin.Default()

	router.Use(func(c *gin.Context) {
		c.Next()
	})

	// Open Database
	db, err := reveil.NewLevelDBStorage(cfg.LevelDBPath)
	if err != nil {
		log.Fatal("Unable to open the database:", err)
	}

	// Prepare struct
	app := &App{
		cfg:    cfg,
		db:     db,
		router: router,
	}

	// Register routes
	ui.DeclareRoutes(router, cfg)
	ui.DeclareNoJSRoutes(router, cfg, db, app.ResetTimer)
	api.DeclareRoutes(router, cfg, db, app.ResetTimer)

	router.GET("/api/version", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"version": Version})
	})

	return app
}

func (app *App) Start() {
	app.srv = &http.Server{
		Addr:    app.cfg.Bind,
		Handler: app.router,
	}

	app.ResetTimer()

	log.Printf("Ready, listening on %s\n", app.cfg.Bind)
	if err := app.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("listen: %s\n", err)
	}
}

func (app *App) ResetTimer() {
	if app.nextAlarm != nil {
		app.nextAlarm.Stop()
		app.nextPreAlarm = nil
		app.nextAlarm = nil
	}

	settings, _ := reveil.ReadSettings(app.cfg.SettingsFile)

	if na, routines, federated, err := reveil.GetNextAlarm(app.cfg, app.db); err == nil && na != nil {
		if settings != nil && settings.PreAlarmAction != "" {
			app.nextPreAlarm = time.AfterFunc(time.Until(*na)-settings.PreAlarmActionDelay*time.Minute, func() {
				app.nextPreAlarm = nil

				settings, err := reveil.ReadSettings(app.cfg.SettingsFile)
				if err != nil {
					log.Println("Unable to read settings:", err.Error())
					return
				}

				action, err := reveil.LoadAction(app.cfg, settings.PreAlarmAction)
				if err != nil {
					log.Println("Unable to load pre-alarm action:", err.Error())
				}

				cmd, err := action.Launch(settings)
				if err != nil {
					log.Println(err.Error())
					return
				}

				go func() {
					err := cmd.Wait()
					if err != nil {
						log.Printf("%q: %s", action.Name, err.Error())
					}
				}()
			})
			log.Println("Next pre-alarm programmed for", time.Time(*na).Add(settings.PreAlarmActionDelay*-1*time.Minute))
		}
		app.nextAlarm = time.AfterFunc(time.Until(*na), func() {
			app.nextPreAlarm = nil
			app.nextAlarm = nil
			reveil.RemoveOldAlarmsSingle(app.db)

			// Rearm timer for the next time
			app.ResetTimer()

			err := player.WakeUp(app.cfg, routines, federated)
			if err != nil {
				log.Println(err.Error())
				return
			}
		})
		log.Println("Next timer programmed for", *na)
	}
}

func (app *App) Stop() {
	if app.nextAlarm != nil {
		app.nextAlarm.Stop()
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := app.srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	if err := app.db.Close(); err != nil {
		log.Fatal("Database Close:", err)
	}
}
