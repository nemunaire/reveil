package config

import (
	"encoding/base64"
	"net/url"
	"time"
)

type JWTSecretKey []byte

func (i *JWTSecretKey) String() string {
	return base64.StdEncoding.EncodeToString(*i)
}

func (i *JWTSecretKey) Set(value string) error {
	z, err := base64.StdEncoding.DecodeString(value)
	if err != nil {
		return err
	}

	*i = z
	return nil
}

type URL struct {
	URL *url.URL
}

func (i *URL) String() string {
	if i.URL != nil {
		return i.URL.String()
	} else {
		return ""
	}
}

func (i *URL) Set(value string) error {
	u, err := url.Parse(value)
	if err != nil {
		return err
	}

	i.URL = u
	return nil
}

type Timezone struct {
	tz *time.Location
}

func (tz *Timezone) GetLocation() *time.Location {
	if tz.tz != nil {
		return tz.tz
	} else {
		return time.Local
	}
}

func (tz *Timezone) String() string {
	if tz.tz != nil {
		return tz.tz.String()
	} else {
		return time.Local.String()
	}
}

func (tz *Timezone) Set(value string) error {
	newtz, err := time.LoadLocation(value)
	if err != nil {
		return err
	}

	tz.tz = newtz
	return nil
}
