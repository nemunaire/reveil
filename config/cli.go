package config

import (
	"flag"
)

// declareFlags registers flags for the structure Options.
func (c *Config) declareFlags() {
	flag.Var(&c.ExternalURL, "external-url", "Public URL of the service")
	flag.StringVar(&c.BaseURL, "baseurl", c.BaseURL, "URL prepended to each URL")
	flag.StringVar(&c.Bind, "bind", c.Bind, "Bind port/socket")
	flag.StringVar(&c.DevProxy, "dev", c.DevProxy, "Use ui directory instead of embedded assets")
	flag.StringVar(&c.LevelDBPath, "leveldb-path", c.LevelDBPath, "Path to the LevelDB database")
	flag.StringVar(&c.SettingsFile, "settings-file", c.SettingsFile, "Path to the file containing the settings")
	flag.StringVar(&c.TracksDir, "tracks-dir", c.TracksDir, "Path to the directory containing the tracks")
	flag.StringVar(&c.GongsDir, "gongs-dir", c.GongsDir, "Path to the directory containing the gongs")
	flag.StringVar(&c.ActionsDir, "actions-dir", c.ActionsDir, "Path to the directory containing the actions")
	flag.StringVar(&c.RoutinesDir, "routines-dir", c.RoutinesDir, "Path to the directory containing the routines")
	flag.IntVar(&c.SampleRate, "samplerate", c.SampleRate, "Samplerate for unifying output stream")

	// Others flags are declared in some other files when they need specials configurations
}

func Consolidated() (cfg *Config, err error) {
	// Define defaults options
	cfg = &Config{
		Bind:         "127.0.0.1:8080",
		LevelDBPath:  "alarms.db",
		SettingsFile: "./settings.json",
		TracksDir:    "./tracks/",
		GongsDir:     "./gongs/",
		ActionsDir:   "./actions/",
		RoutinesDir:  "./routines/",
		SampleRate:   44100,
	}

	cfg.declareFlags()

	// Then, overwrite that by what is present in the environment
	err = cfg.FromEnv()
	if err != nil {
		return
	}

	// Finaly, command line takes precedence
	err = cfg.parseCLI()
	if err != nil {
		return
	}

	return
}

// parseCLI parse the flags and treats extra args as configuration filename.
func (c *Config) parseCLI() error {
	flag.Parse()

	return nil
}
