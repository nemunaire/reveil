package reveil

import (
	"encoding/base64"
	"errors"
)

const IDENTIFIER_LEN = 48

type Identifier []byte

func NewIdentifierFromString(src string) (id Identifier, err error) {
	return base64.RawURLEncoding.DecodeString(src)
}

func (i *Identifier) IsEmpty() bool {
	return len(*i) == 0
}

func (i *Identifier) ToString() string {
	return base64.RawURLEncoding.EncodeToString(*i)
}

func (i Identifier) MarshalJSON() (dst []byte, err error) {
	dst = make([]byte, base64.RawURLEncoding.EncodedLen(len(i)))
	base64.RawURLEncoding.Encode(dst, i)
	dst = append([]byte{'"'}, dst...)
	dst = append(dst, '"')
	return
}

func (i *Identifier) UnmarshalJSON(src []byte) error {
	if len(src) < 2 || src[0] != '"' || src[len(src)-1] != '"' {
		return errors.New("Unvalid character found to encapsulate the JSON value")
	}

	*i = make([]byte, base64.RawURLEncoding.DecodedLen(len(src)-2))
	_, err := base64.RawURLEncoding.Decode(*i, src[1:len(src)-1])

	return err
}
