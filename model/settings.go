package reveil

import (
	"encoding/json"
	"os"
	"time"
)

// Settings represents the settings panel.
type Settings struct {
	Language            string                      `json:"language"`
	GongInterval        time.Duration               `json:"gong_interval"`
	WeatherDelay        time.Duration               `json:"weather_delay"`
	WeatherAction       string                      `json:"weather_action"`
	PreAlarmActionDelay time.Duration               `json:"pre_alarm_delay"`
	PreAlarmAction      string                      `json:"pre_alarm_action"`
	MaxRunTime          time.Duration               `json:"max_run_time"`
	MaxVolume           uint16                      `json:"max_volume"`
	Federation          map[string]FederationServer `json:"federation"`
}

// ExistsSettings checks if the settings file can by found at the given path.
func ExistsSettings(settingsPath string) bool {
	_, err := os.Stat(settingsPath)
	return !os.IsNotExist(err)
}

// ReadSettings parses the file at the given location.
func ReadSettings(path string) (*Settings, error) {
	var s Settings
	if fd, err := os.Open(path); err != nil {
		return nil, err
	} else {
		defer fd.Close()
		jdec := json.NewDecoder(fd)

		if err := jdec.Decode(&s); err != nil {
			return &s, err
		}

		return &s, nil
	}
}

// SaveSettings saves settings at the given location.
func SaveSettings(path string, s interface{}) error {
	if fd, err := os.Create(path); err != nil {
		return err
	} else {
		defer fd.Close()
		jenc := json.NewEncoder(fd)

		if err := jenc.Encode(s); err != nil {
			return err
		}

		return nil
	}
}
