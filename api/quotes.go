package api

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/config"
)

var quotes = []Quote{
	Quote{1, "La qualité d’un homme se calcule à sa démesure ; tentez, essayer, échouez même, ce sera votre réussite.", "Jacques brel"},
	Quote{2, "Certains veulent que ça arrive, d’autres aimeraient que ça arrive, et les autres font que ça arrive.", "Michael Jordan"},
	Quote{3, "Les winners sont des loosers qui se lèvent et essaient une fois de plus.", "Dennis DeYoung"},
	Quote{4, "L’homme le plus heureux est celui qui fait le bonheur d’un plus grand nombre d’autres.", "Denis Diderot"},
	Quote{5, "La joie est en tout ; il faut savoir l’extraire.", "Confucius"},
	Quote{6, "L’optimiste ne refuse jamais de voir le côté négatif des choses ; il refuse simplement de s’attarder dessus.", "Alexandre Lockhart"},
	Quote{7, "L’obstination est le chemin de la réussite.", "Charlie chaplin"},
	Quote{8, "Si on veut obtenir quelque chose que l’on n’a jamais eu, il faut tenter quelque chose que l’on n’a jamais fait.", "Péricles"},
	Quote{9, "La plus grande erreur que puisse faire un homme est d’avoir peur d’en faire une.", "Elbert Hubbard"},
	Quote{10, "La définition de la folie, c’est de refaire toujours la même chose, et d’attendre un résultat différent.", "Albert Einstein"},
	Quote{11, "Peu importe qui tu es ou qui tu as été, tu peux être qui tu veux.", "Clement Stone"},
	Quote{12, "Celui qui attend que tout danger soir écarté pour mettre les voiles ne prendra jamais la mer.", "Thomas Fuller"},
	Quote{13, "Beaucoup de ceux qui ont échoué n’ont pas réalisé qu’ils étaient aussi près du succès quand ils ont abandonné.", "Thomas Edison"},
	Quote{14, "La vie, c’est comme une bicyclette, il faut avancer pour ne pas perdre l’équilibre.", "Albert Einstein"},
	Quote{15, "Vous ne pouvez choisir ni comment mourir, ni quand. Mais vous pouvez décider de comment vous allez vivre. Maintenant.", "Joan Baez"},
	Quote{16, "Je passe mon temps à faire ce que je ne sais pas faire, pour apprendre à le faire.", "Pablo Picasso"},
	Quote{17, "Le dictionnaire, c’est le seul endroit où Succès arrive avant Travail.", "Vince Lombardi"},
	Quote{18, "Si on me donnait six heures pour abattre un arbre, je passerais la première à affûter la hache.", "Abraham Lincoln"},
	Quote{19, "A la fin, ce qui compte, ce ne sont pas les années qu’il y a eu dans la vie. C’est la vie qu’il y a eu dans les années.", "Abraham Lincoln"},
	Quote{20, "La logique peut vous mener d’un point A à un point B. L’imagination peut vous mener partout.", "Albert Einstein"},
	Quote{21, "N’allez pas où va le chemin. Allez là où il n’y en a pas encore, et ouvrez la route.", "Ralph Waldo Emerson"},
	Quote{22, "Un homme doit être assez grand pour admettre ses erreurs, assez intelligent pour apprendre de celles-ci et assez fort pour les corriger. John C.", "Maxwell"},
	Quote{23, "Si ce que vous faites ne vous rapproche pas de vos buts, alors c’est que ça vous éloigne de ceux-ci.", "Brian Tracy"},
	Quote{24, "Qui veut faire quelque chose trouve un moyen, qui ne veut rien faire trouve une excuse.", "Proverbe arabe"},
	Quote{25, "Si vous voulez que la vie vous sourie, apportez-lui votre bonne humeur.", "Baruch Spinoza"},
	Quote{26, "Nulle pierre ne peut être polie sans friction, nul homme ne peut parfaire son expérience sans épreuve.", "Confucius"},
	Quote{27, "Se donner du mal pour les petites choses, c’est parvenir aux grandes, avec le temps.", "Samuel Beckett"},
	Quote{28, "Le succès c’est d’avoir ce que vous désirez. Le bonheur c’est aimer ce que vous avez. H.", "Jackson Brown"},
	Quote{29, "La chose la plus difficile est de n’attribuer aucune importance aux choses qui n’ont aucune importance.", "Charles de Gaulle"},
	Quote{30, "Nous commençons à vieillir quand nous remplaçons nos rêves par des regrets.", "Sénèque"},
	Quote{31, "Je préfère vivre en optimiste et me tromper, que vivre en pessimiste pour la seule satisfaction d’avoir eu raison.", "Milan Kundera"},
	Quote{32, "Les optimistes proclament que nous vivons dans un monde rempli de possibilités… Les pessimistes ont peur que ce soit vrai !", "James Branch Cabell"},
	Quote{33, "L’une des meilleures façons d’aider quelqu’un est de lui donner une responsabilité et de lui faire savoir que vous lui faites confiance.", "Booker Washington"},
	Quote{34, "La seule limite à notre épanouissement de demain sera nos doutes d’aujourd’hui.", "Franklin Roosevelt"},
	Quote{35, "C’est dans les moments les plus sombres qu’on voit le mieux les étoiles.", "Charles Beard"},
	Quote{36, "Ne jugez pas chaque journée par votre récolte, mais par les graines que vous avez plantées.", "Robert Stevenson"},
	Quote{37, "Contentez-vous d’agir et laissez les autres parler.", "Baltasar Gracian"},
	Quote{38, "Mettez en tout un grain d’audace.", "Baltazar Gracian"},
	Quote{39, "Un sourire coûte moins cher que l’électricité, mais donne autant de lumière.", "Abbé Pierre"},
	Quote{40, "N’attendez pas d’être heureux pour sourire, souriez pour être heureux.", "Edward Kramer"},
	Quote{41, "Tout le monde est un génie. Mais si on juge un poisson sur sa capacité à grimper à un arbre, il passera sa vie à croire qu’il est stupide.", "Albert Einstein"},
	Quote{42, "Le succès est la capacité d’aller d’échec en échec sans perdre son enthousiasme.", "Winston Churchill"},
	Quote{43, "Rien ne sert de défendre le monde d’hier quand on peut construire le monde de demain.", "Peter Drucker"},
	Quote{44, "Le plus grand plaisir de la vie est de réaliser ce que les autres vous pensent incapable de réaliser.", "Walter Bagehot"},
	Quote{45, "Nous avons deux choix dans la vie : le premier est d’accepter les choses comme elles sont et la deuxième est de prendre la décision de les changer.", "Denis Waitley"},
	Quote{46, "N’acceptez jamais la défaite, vous êtes peut-être à un pas de la réussite.", "Jack E Addington"},
	Quote{47, "L’échec est seulement l’opportunité de recommencer d’une façon plus intelligente.", "Henry Ford"},
	Quote{48, "Il n’y a qu’une façon d’échouer, c’est d’abandonner avant d’avoir réussi.", "Georges Clemenceau"},
	Quote{49, "Vous n’avez rien à craindre car l’échec est impossible. Vous ne pouvez qu’apprendre, évoluer et devenir meilleur que vous ne l’avez jamais été.", "Hal Elrod"},
	Quote{50, "Appréciez d’échouer, et apprenez de l’échec, car on n’apprend rien de ses succès.", "James Dyson"},
	Quote{51, "Si vous vivez un moment difficile, ne blâmez pas la vie. Vous êtes juste en train de devenir plus fort.", "Gandhi"},
	Quote{52, "Au milieu de toute difficulté se trouve cachée une opportunité.", "Albert Einstein"},
	Quote{53, "L’échec est l’épice qui donne sa saveur au succès.", "Truman Capote"},
	Quote{54, "Je n’ai pas échoué. J’ai simplement trouvé 10 000 façons de ne pas y arriver.", "Thomas Edison"},
	Quote{55, "Les plus belles années d’une vie sont celles que l’on n’a pas encore vécues.", "Victor Hugo"},
	Quote{56, "Si vous voulez que la vie vous sourie, apportez-lui d’abord votre bonne humeur.", "Baruch Spinoza"},
	Quote{57, "Ce n’est pas parce que les choses sont difficiles que nous n’osons pas, c’est parce que nous n’osons pas qu’elles sont difficiles.", "Sénèque"},
	Quote{58, "Si on veut obtenir quelque chose que l’on n’a jamais eu, il faut tenter quelque chose que l’on n’a jamais fait.", "Périclès"},
	Quote{59, "Accepte ce qui est, laisse aller ce qui était, aie confiance en ce qui sera.", "Bouddha"},
	Quote{60, "Choisis un travail que tu aimes, tu n’auras pas à travailler un seul jour de ta vie.", "Confucius"},
	Quote{61, "Il est de loin plus lucratif et plus amusant de capitaliser sur vos points forts que d’essayer de corriger tous vos points faibles.", "Tim Ferriss"},
	Quote{62, "Un homme ayant du succès est celui pouvant se construire une ferme fondation avec les briques que les autres lui jettent.", "David Brinkley"},
	Quote{63, "Ils ne savaient pas que c’était impossible alors ils l’ont fait.", "Mark Twain"},
	Quote{64, "Les gagnants trouvent des moyens, les perdants des excuses.", "Franklin Roosevelt"},
	Quote{65, "Croyez en vos rêves et ils se réaliseront peut-être. Croyez en vous et ils se réaliseront sûrement.", "Martin Luther King"},
	Quote{66, "Un voyage de mille lieues commence toujours par un premier pas.", "Lao Tseu"},
	Quote{67, "Il faut toujours viser la lune car même en cas d’échec on atterrit dans les étoiles.", "Oscar Wilde"},
	Quote{68, "Ce qui est plus triste qu’une œuvre inachevée, c’est une œuvre jamais commencée.", "Christinna Rosseti"},
	Quote{69, "L’obscurité ne peut pas chasser l’obscurité, seule la lumière le peut. La haine ne peut pas chasser la haine, seul l’amour le peut.", "Martin Luther King"},
	Quote{70, "Je n’ai pas peur de demain, car j’ai vu hier et j’aime aujourd’hui.", "William Allen White"},
	Quote{71, "Un pessimiste voit la difficulté dans chaque opportunité, un optimiste voit l’opportunité dans chaque difficulté.", "Winston Churchill"},
	Quote{72, "Si vous regardez avec attention, la plupart des succès obtenus du jour au lendemain prennent beaucoup de temps.", "Steve Jobs"},
	Quote{73, "Les échecs sont les marches que nous montons pour atteindre le succès.", "Roy Bennett"},
	Quote{74, "Si vous pouvez le rêver, vous pouvez le faire.", "Walt Disney"},
	Quote{75, "Croyez en vos rêves et ils se réaliseront peut-être. Croyez en vous, et ils se réaliseront sûrement.", "Martin Luther King"},
	Quote{76, "La première étape est de dire que tu peux.", "Will Smith"},
	Quote{77, "Quand on veut une chose, tout l’Univers conspire à nous permettre de réaliser notre rêve.", "Paulo Coelho"},
	Quote{78, "Tout est possible à qui rêve, ose, travaille et n’abandonne jamais.", "Xavier Dolan"},
	Quote{79, "La sagesse, c’est d’avoir des rêves suffisamment grands pour ne pas les perdre de vue lorsqu’on les poursuit.", "Oscar Wilde"},
	Quote{80, "Quand vous osez rêver grand, c’est là où votre système nerveux va créer du plaisir et être orienté solutions.", "David Laroche"},
	Quote{81, "Choisir sa vie, c’est se demander si l’on doit réaliser ses rêves ou subir le quotidien.", "Sonia Lahsaini"},
	Quote{82, "Fais de ta vie un rêve, et d’un rêve, une réalité.", "Antoine de Saint-Exupéry"},
}

func declareQuotesRoutes(cfg *config.Config, router *gin.RouterGroup) {
	router.GET("/quoteoftheday", func(c *gin.Context) {
		c.JSON(http.StatusOK, quotes[time.Now().Add(-5*time.Hour).YearDay()%len(quotes)])
	})

	router.GET("/quotes", func(c *gin.Context) {
		c.JSON(http.StatusOK, quotes)
	})
	router.POST("/quotes", func(c *gin.Context) {

	})

	quotesRoutes := router.Group("/quotes/:qid")
	quotesRoutes.Use(quoteHandler)

	quotesRoutes.GET("", func(c *gin.Context) {
		c.JSON(http.StatusOK, c.MustGet("quote"))
	})
	quotesRoutes.PUT("", func(c *gin.Context) {
		c.JSON(http.StatusOK, c.MustGet("quote"))
	})
	quotesRoutes.DELETE("", func(c *gin.Context) {
		c.JSON(http.StatusOK, c.MustGet("quote"))
	})
}

func quoteHandler(c *gin.Context) {
	c.Set("quote", nil)

	c.Next()
}

type Quote struct {
	Id      int    `json:"id"`
	Content string `json:"content"`
	Author  string `json:"author"`
}
