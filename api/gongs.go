package api

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/config"
	"git.nemunai.re/nemunaire/reveil/model"
)

func declareGongsRoutes(cfg *config.Config, router *gin.RouterGroup) {
	router.GET("/gongs", func(c *gin.Context) {
		gongs, err := reveil.LoadGongs(cfg)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gongs)
	})
	router.POST("/gongs", func(c *gin.Context) {
		fgong, err := c.FormFile("gongfile")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "No gong found"})
			return
		}

		// Check file extension
		if path.Ext(fgong.Filename) != ".mp3" && path.Ext(fgong.Filename) != ".flac" && path.Ext(fgong.Filename) != ".wav" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Bad file type. You should only upload .mp3, .wav or .flac files."})
			return
		}

		if strings.Contains(fgong.Filename, "/") {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Bad file name."})
			return
		}

		dst := path.Join(cfg.GongsDir, fgong.Filename)
		err = c.SaveUploadedFile(fgong, dst)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Something goes wrong when saving the gong: %s", err.Error())})
			return
		}

		d, err := os.Stat(dst)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Something goes wrong when saving the gong: %s", err.Error())})
			return
		}

		gong, err := reveil.LoadGong(cfg, dst, d)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to load gong: %s", err.Error())})
			return
		}

		c.JSON(http.StatusOK, gong)
	})

	gongsRoutes := router.Group("/gongs/:tid")
	gongsRoutes.Use(func(c *gin.Context) {
		gongs, err := reveil.LoadGongs(cfg)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		for _, g := range gongs {
			if g.Id.ToString() == c.Param("tid") {
				c.Set("gong", g)
				c.Next()
				return
			}
		}

		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": "Gong not found"})
	})

	gongsRoutes.GET("", func(c *gin.Context) {
		c.JSON(http.StatusOK, c.MustGet("gong"))
	})
	gongsRoutes.GET("/stream", func(c *gin.Context) {
		gong := c.MustGet("gong").(*reveil.Gong)

		size, err := gong.Size()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to open the gong: %s", err.Error())})
			return
		}

		fd, err := gong.Open()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to open the gong: %s", err.Error())})
			return
		}
		defer fd.Close()

		c.DataFromReader(http.StatusOK, size, gong.ContentType(), fd, map[string]string{})
	})
	gongsRoutes.PUT("", func(c *gin.Context) {
		oldgong := c.MustGet("gong").(*reveil.Gong)

		var gong reveil.Gong
		if err := c.ShouldBindJSON(&gong); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		if gong.Name != oldgong.Name {
			err := oldgong.Rename(gong.Name)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to rename the gong: %s", err.Error())})
				return
			}
		}

		if gong.Enabled != oldgong.Enabled {
			var err error
			if gong.Enabled {
				err = oldgong.SetDefault(cfg)
			}

			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to set the new default gong: %s", err.Error())})
				return
			}
		}

		c.JSON(http.StatusOK, oldgong)
	})
	gongsRoutes.DELETE("", func(c *gin.Context) {
		gong := c.MustGet("gong").(*reveil.Gong)

		err := gong.Remove()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to remove the gong: %s", err.Error())})
			return
		}

		c.JSON(http.StatusOK, nil)
	})
}
