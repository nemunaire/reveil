package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/config"
	"git.nemunai.re/nemunaire/reveil/model"
)

func declareAlarmsRoutes(cfg *config.Config, db *reveil.LevelDBStorage, resetTimer func(), router *gin.RouterGroup) {
	router.GET("/alarms/next", func(c *gin.Context) {
		alarm, _, _, err := reveil.GetNextAlarm(cfg, db)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, alarm)
	})

	router.DELETE("/alarms/next", func(c *gin.Context) {
		err := reveil.DropNextAlarm(cfg, db)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, true)
	})

	router.GET("/alarms/single", func(c *gin.Context) {
		alarms, err := reveil.GetAlarmsSingle(db)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, alarms)
	})
	router.POST("/alarms/single", func(c *gin.Context) {
		var alarm reveil.AlarmSingle
		if err := c.ShouldBindJSON(&alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		if time.Now().After(alarm.Time) {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "This date is already passed."})
			return
		}

		alarm.Id = nil
		if err := reveil.PutAlarmSingle(db, &alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, alarm)
	})

	router.GET("/alarms/repeated", func(c *gin.Context) {
		alarms, err := reveil.GetAlarmsRepeated(db)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, alarms)
	})
	router.POST("/alarms/repeated", func(c *gin.Context) {
		var alarm reveil.AlarmRepeated
		if err := c.ShouldBindJSON(&alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		alarm.Id = nil
		if err := reveil.PutAlarmRepeated(db, &alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, alarm)
	})

	router.GET("/alarms/exceptions", func(c *gin.Context) {
		exceptions, err := reveil.GetAlarmExceptions(db)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, exceptions)
	})
	router.POST("/alarms/exceptions", func(c *gin.Context) {
		var alarm reveil.AlarmException
		if err := c.ShouldBindJSON(&alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		if alarm.Start == nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Start not defined"})
			return
		}
		if alarm.End == nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "End not defined"})
			return
		}
		if time.Now().After(time.Time(*alarm.End)) {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "End date is already passed."})
			return
		}
		if time.Time(*alarm.Start).After(time.Time(*alarm.End)) {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Start is defined after End. Please verify your inputs."})
			return
		}

		alarm.Id = nil
		if err := reveil.PutAlarmException(db, &alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, alarm)
	})

	singleAlarmsRoutes := router.Group("/alarms/single/:aid")
	singleAlarmsRoutes.Use(func(c *gin.Context) {
		id, err := reveil.NewIdentifierFromString(c.Param("aid"))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": fmt.Sprintf("Invalid alarm idenfifier: %s", err.Error())})
			return
		}

		alarm, err := reveil.GetAlarmSingle(db, id)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": err.Error()})
			return
		}

		c.Set("alarm", alarm)

		c.Next()
	})

	singleAlarmsRoutes.GET("", func(c *gin.Context) {
		c.JSON(http.StatusOK, c.MustGet("alarm"))
	})
	singleAlarmsRoutes.PUT("", func(c *gin.Context) {
		oldalarm := c.MustGet("alarm").(*reveil.AlarmSingle)

		var alarm reveil.AlarmSingle
		if err := c.ShouldBindJSON(&alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		alarm.Id = oldalarm.Id
		if err := reveil.PutAlarmSingle(db, &alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, alarm)
	})
	singleAlarmsRoutes.DELETE("", func(c *gin.Context) {
		alarm := c.MustGet("alarm").(*reveil.AlarmSingle)

		if err := reveil.DeleteAlarmSingle(db, alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, nil)
	})

	repeatedAlarmsRoutes := router.Group("/alarms/repeated/:aid")
	repeatedAlarmsRoutes.Use(func(c *gin.Context) {
		id, err := reveil.NewIdentifierFromString(c.Param("aid"))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": fmt.Sprintf("Invalid alarm idenfifier: %s", err.Error())})
			return
		}

		alarm, err := reveil.GetAlarmRepeated(db, id)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": err.Error()})
			return
		}

		c.Set("alarm", alarm)

		c.Next()
	})

	repeatedAlarmsRoutes.GET("", func(c *gin.Context) {
		alarm := c.MustGet("alarm").(*reveil.AlarmRepeated)
		alarm.FillExcepts(cfg, db)
		alarm.NextTime = alarm.GetNextOccurence(cfg, db)

		c.JSON(http.StatusOK, alarm)
	})
	repeatedAlarmsRoutes.PUT("", func(c *gin.Context) {
		oldalarm := c.MustGet("alarm").(*reveil.AlarmRepeated)

		var alarm reveil.AlarmRepeated
		if err := c.ShouldBindJSON(&alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		alarm.Id = oldalarm.Id
		if err := reveil.PutAlarmRepeated(db, &alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, alarm)
	})
	repeatedAlarmsRoutes.DELETE("", func(c *gin.Context) {
		alarm := c.MustGet("alarm").(*reveil.AlarmRepeated)

		if err := reveil.DeleteAlarmRepeated(db, alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, nil)
	})

	exceptionAlarmsRoutes := router.Group("/alarms/exceptions/:aid")
	exceptionAlarmsRoutes.Use(func(c *gin.Context) {
		id, err := reveil.NewIdentifierFromString(c.Param("aid"))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": fmt.Sprintf("Invalid alarm idenfifier: %s", err.Error())})
			return
		}

		alarm, err := reveil.GetAlarmException(db, id)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": err.Error()})
			return
		}

		c.Set("alarm", alarm)

		c.Next()
	})

	exceptionAlarmsRoutes.GET("", func(c *gin.Context) {
		c.JSON(http.StatusOK, c.MustGet("alarm"))
	})
	exceptionAlarmsRoutes.PUT("", func(c *gin.Context) {
		oldalarm := c.MustGet("alarm").(*reveil.AlarmException)

		var alarm reveil.AlarmException
		if err := c.ShouldBindJSON(&alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		alarm.Id = oldalarm.Id
		if err := reveil.PutAlarmException(db, &alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, alarm)
	})
	exceptionAlarmsRoutes.DELETE("", func(c *gin.Context) {
		alarm := c.MustGet("alarm").(*reveil.AlarmException)

		if err := reveil.DeleteAlarmException(db, alarm); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		resetTimer()

		c.JSON(http.StatusOK, nil)
	})
}
