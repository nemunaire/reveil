package api

import (
	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/config"
	"git.nemunai.re/nemunaire/reveil/model"
)

func DeclareRoutes(router *gin.Engine, cfg *config.Config, db *reveil.LevelDBStorage, resetTimer func()) {
	apiRoutes := router.Group("/api")

	declareActionsRoutes(cfg, apiRoutes)
	declareAlarmRoutes(cfg, apiRoutes)
	declareAlarmsRoutes(cfg, db, resetTimer, apiRoutes)
	declareFederationRoutes(cfg, apiRoutes)
	declareGongsRoutes(cfg, apiRoutes)
	declareHistoryRoutes(cfg, apiRoutes)
	declareQuotesRoutes(cfg, apiRoutes)
	declareRoutinesRoutes(cfg, apiRoutes)
	declareTracksRoutes(cfg, apiRoutes)
	declareSettingsRoutes(cfg, apiRoutes)
}
