package api

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/config"
	"git.nemunai.re/nemunaire/reveil/model"
)

func declareSettingsRoutes(cfg *config.Config, router *gin.RouterGroup) {
	router.GET("/settings", func(c *gin.Context) {
		settings, err := reveil.ReadSettings(cfg.SettingsFile)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, settings)
	})
	router.PUT("/settings", func(c *gin.Context) {
		var config reveil.Settings
		err := c.ShouldBindJSON(&config)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		err = reveil.SaveSettings(cfg.SettingsFile, config)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, config)
	})
}
