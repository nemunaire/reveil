package api

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/config"
	"git.nemunai.re/nemunaire/reveil/model"
	"git.nemunai.re/nemunaire/reveil/player"
)

func declareAlarmRoutes(cfg *config.Config, router *gin.RouterGroup) {
	router.GET("/alarm", func(c *gin.Context) {
		if player.CommonPlayer == nil {
			c.JSON(http.StatusOK, false)
		} else {
			c.JSON(http.StatusOK, true)
		}
	})

	router.POST("/alarm/run", func(c *gin.Context) {
		if player.CommonPlayer == nil {
			err := player.WakeUp(cfg, nil, true)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
				return
			}
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Player already running"})
			return
		}

		c.JSON(http.StatusOK, true)
	})

	router.POST("/alarm/next", func(c *gin.Context) {
		if player.CommonPlayer == nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "No player currently playing"})
			return
		} else {
			player.CommonPlayer.NextTrack()
		}

		c.JSON(http.StatusOK, true)
	})

	router.DELETE("/alarm", func(c *gin.Context) {
		if player.CommonPlayer != nil {
			err := player.CommonPlayer.Stop()
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
				return
			}

			settings, err := reveil.ReadSettings(cfg.SettingsFile)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Errorf("Unable to read settings: %s", err.Error())})
				return
			}

			for k, srv := range settings.Federation {
				err = srv.WakeStop()
				if err != nil {
					log.Printf("Unable to do federated wakeup on %s: %s", k, err.Error())
				} else {
					log.Printf("Federated wakeup on %s: launched!", k)
				}
			}
		}

		c.JSON(http.StatusOK, true)
	})
}
