package api

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/gin-gonic/gin"

	"git.nemunai.re/nemunaire/reveil/config"
	"git.nemunai.re/nemunaire/reveil/model"
)

func declareTracksRoutes(cfg *config.Config, router *gin.RouterGroup) {
	router.GET("/tracks", func(c *gin.Context) {
		tracks, err := reveil.LoadTracks(cfg)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.JSON(http.StatusOK, tracks)
	})
	router.POST("/tracks", func(c *gin.Context) {
		ftrack, err := c.FormFile("trackfile")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "No track found"})
			return
		}

		// Check file extension
		if path.Ext(ftrack.Filename) != ".mp3" && path.Ext(ftrack.Filename) != ".flac" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Bad file type. You should only upload .mp3 or .flac files."})
			return
		}

		if strings.Contains(ftrack.Filename, "/") {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Bad file name."})
			return
		}

		dst := path.Join(cfg.TracksDir, ftrack.Filename)
		err = c.SaveUploadedFile(ftrack, dst)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Something goes wrong when saving the track: %s", err.Error())})
			return
		}

		d, err := os.Stat(dst)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Something goes wrong when saving the track: %s", err.Error())})
			return
		}

		track, err := reveil.LoadTrack(dst, d)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to load track: %s", err.Error())})
			return
		}

		c.JSON(http.StatusOK, track)
	})

	tracksRoutes := router.Group("/tracks/:tid")
	tracksRoutes.Use(func(c *gin.Context) {
		tracks, err := reveil.LoadTracks(cfg)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		for _, t := range tracks {
			if t.Id.ToString() == c.Param("tid") {
				c.Set("track", t)
				c.Next()
				return
			}
		}

		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": "Track not found"})
	})

	tracksRoutes.GET("", func(c *gin.Context) {
		c.JSON(http.StatusOK, c.MustGet("track"))
	})
	tracksRoutes.GET("/stream", func(c *gin.Context) {
		track := c.MustGet("track").(*reveil.Track)

		size, err := track.Size()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to open the track: %s", err.Error())})
			return
		}

		fd, err := track.Open()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to open the track: %s", err.Error())})
			return
		}
		defer fd.Close()

		c.DataFromReader(http.StatusOK, size, track.ContentType(), fd, map[string]string{})
	})
	tracksRoutes.PUT("", func(c *gin.Context) {
		oldtrack := c.MustGet("track").(*reveil.Track)

		var track reveil.Track
		if err := c.ShouldBindJSON(&track); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		if track.Name != oldtrack.Name {
			err := oldtrack.Rename(track.Name)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to rename the track: %s", err.Error())})
				return
			}
		}

		if track.Enabled != oldtrack.Enabled {
			var err error
			if track.Enabled {
				err = oldtrack.Enable(cfg)
			} else {
				err = oldtrack.Disable()
			}

			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to enable/disable the track: %s", err.Error())})
				return
			}
		}

		c.JSON(http.StatusOK, oldtrack)
	})
	tracksRoutes.DELETE("", func(c *gin.Context) {
		track := c.MustGet("track").(*reveil.Track)

		err := track.Remove()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to remove the track: %s", err.Error())})
			return
		}

		c.JSON(http.StatusOK, nil)
	})
}
