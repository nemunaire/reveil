FROM node:22-alpine as nodebuild

WORKDIR /ui

COPY ui/ .

RUN npm install --network-timeout=100000 && \
    npm run build


FROM golang:1-alpine AS build

RUN apk --no-cache add git go-bindata

COPY . /go/src/git.nemunai.re/nemunaire/reveil
COPY --from=nodebuild /ui/build /go/src/git.nemunai.re/nemunaire/reveil/ui/build
WORKDIR /go/src/git.nemunai.re/nemunaire/reveil
RUN go get -v && go generate -v && go build -tags pulse -ldflags="-s -w"


FROM alpine:3.21

VOLUME /data
WORKDIR /data

EXPOSE 8080
CMD ["/srv/reveil"]

COPY --from=build /go/src/git.nemunai.re/nemunaire/reveil/reveil /srv/reveil
