package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"git.nemunai.re/nemunaire/reveil/config"
)

var (
	Version = "custom-build"
)

func main() {
	cfg, err := config.Consolidated()
	if err != nil {
		log.Fatal("Unable to read configuration:", err)
	}

	// Clean paths
	if _, err := os.Stat(cfg.SettingsFile); os.IsNotExist(err) {
		fd, err := os.Create(cfg.SettingsFile)
		if err != nil {
			log.Fatal("Unable to create settings file:", err)
		}

		fd.Write([]byte{'{', '}'})

		fd.Close()
	}

	if _, err := os.Stat(cfg.TracksDir); os.IsNotExist(err) {
		if err := os.Mkdir(cfg.TracksDir, 0755); err != nil {
			log.Fatal("Unable to create tracks directory:", err)
		}
	}
	if _, err := os.Stat(cfg.GongsDir); os.IsNotExist(err) {
		if err := os.Mkdir(cfg.GongsDir, 0755); err != nil {
			log.Fatal("Unable to create gongs directory:", err)
		}
	}
	if _, err := os.Stat(cfg.ActionsDir); os.IsNotExist(err) {
		if err := os.Mkdir(cfg.ActionsDir, 0755); err != nil {
			log.Fatal("Unable to create actions directory:", err)
		}
	}
	if _, err := os.Stat(cfg.RoutinesDir); os.IsNotExist(err) {
		if err := os.Mkdir(cfg.RoutinesDir, 0755); err != nil {
			log.Fatal("Unable to create routines directory:", err)
		}
	}

	// Start app
	a := NewApp(cfg)
	go a.Start()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	log.Println("Stopping the service...")
	a.Stop()
	log.Println("Stopped")
}
