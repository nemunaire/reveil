//go:build pulse

package player

const (
	playCommand = "paplay"
	mixerCard   = "pulse"
	mixerName   = "Master"
)
