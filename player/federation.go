package player

import (
	"log"
	"time"

	"git.nemunai.re/nemunaire/reveil/model"
)

func FederatedWakeUp(k string, srv reveil.FederationServer, seed int64) {
	if srv.Delay == 0 {
		err := srv.WakeUp(seed)
		if err != nil {
			log.Printf("Unable to do federated wakeup on %s: %s", k, err.Error())
		} else {
			log.Printf("Federated wakeup on %s: launched!", k)
		}
	} else {
		go func() {
			time.Sleep(time.Duration(srv.Delay) * time.Millisecond)
			err := srv.WakeUp(seed)
			if err != nil {
				log.Printf("Unable to do federated wakeup on %s: %s", k, err.Error())
			} else {
				log.Printf("Federated wakeup on %s: launched!", k)
			}
		}()
	}
}
